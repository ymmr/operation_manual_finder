var ManualReloader = {
  //==============================================
  // 実行関数
  //==============================================
  execute: function()
  {
    var root_path = new ActiveXObject("WScript.Shell").CurrentDirectory;
    var fso = new ActiveXObject("Scripting.FileSystemObject");
    var src = root_path + "\\manuals";
    var dist = "js\\data";
    var manuals = [];

    this.refresh_viewing_files(root_path, fso);
    this.create_structure_array(manuals, src, fso, root_path);
    this.create_manuals_file(manuals, dist, fso);

    exalert("Finish");
  },

  //==============================================
  // PDFファイルの削除
  //==============================================
  refresh_viewing_files: function(root_path, fso)
  {
    var dir_name = "viewing";
    var files = new Enumerator(fso.GetFolder(dir_name).files);
    for (files.moveFirst(); !files.atEnd(); files.moveNext()) {
      if(files.item().Name.match(/\.pdf$/)) {
        fso.deleteFile(root_path + "\\" + dir_name + "\\*.pdf");

        return false;
      }
    }
  },

  //==============================================
  // ディレクトリ・ファイル構造の配列を作成
  //==============================================
  create_structure_array: function(manuals, src, fso, root_path, parent_id, depth, id_obj)
  {
    parent_id = parent_id !== undefined ? parent_id : 0;
    depth = depth !== undefined ? ++depth : 0;
    id_obj = id_obj !== undefined ? id_obj : {"id": 0};

    var folder = fso.GetFolder(src);

    this.make_dirs_array(manuals, fso, folder, root_path, parent_id, depth, id_obj);
    this.make_files_array(manuals, fso, folder, root_path, parent_id, depth, id_obj);
  },

  get_obj: function(id_obj, parent_id, sortable_rank, title, depth, path, type, attributes)
  {
    id_obj["id"]++;

    attributes = attributes ? attributes : {};

    return {
      "id": id_obj["id"],
      "parent_id": parent_id,
      "sortable_rank": sortable_rank,
      "title": title,
      "depth": depth,
      "path": path,
      "type": type,
      "attributes": attributes
    };
  },

  get_relative_path: function(root_path, path)
  {
    return path.replace(root_path + "\\", "");
  },

  //-----------------------------
  // ファイルに関する配列の作成
  //-----------------------------
  make_files_array: function(manuals, fso, folder, root_path, parent_id, depth, id_obj)
  {
    var accepts = ["txt", "doc", "docx", "pdf", "ai"];
    var files = new Enumerator(folder.files);
    var file_count = 0;

    for (files.moveFirst(); !files.atEnd(); files.moveNext()) {
      var item = files.item();
      var path = item.Path;
      var relative_path = this.get_relative_path(root_path, path);
      var name_matches = item.Name.match(/(.*)\.(.*)$/);
      var extension = name_matches ? name_matches[2].toLowerCase() : "";
      var view_file_path = "viewing\\" + name_matches[1] + ":" + id_obj["id"] + ".pdf";

      if(array_search(accepts, extension) !== -1) {
        var body = this.get_file_body(extension, fso, path, root_path + "\\" + view_file_path);
        var attributes = {
          "extension": extension,
          "is_supported": body !== null,
          "body": body,
          "view_file_path": extension == "pdf" ? relative_path : view_file_path
        };
        var file_obj = this.get_obj(id_obj, parent_id, file_count, name_matches[1], depth, relative_path, "file", attributes);

        manuals.push(file_obj);
        file_count++;
      }
    }
  },

  // ファイルの本文取得
  get_file_body: function(extension, fso, path, view_file_path)
  {
    switch (extension) {
      case "txt":
        return this.get_txt_content(fso, path, view_file_path);
      case "doc":
      case "docx":
        return this.get_word_content(path, view_file_path);
      case "pdf":
        return "";
      default:
        return null;
    }
  },

  // txtファイルの内容取得
  get_txt_content: function(fso, path, view_file_path)
  {
    var stream = new ActiveXObject("ADODB.Stream");
    stream.Type = 2;
    stream.charset = "UTF-8";
    // stream.charset = "Shift_JIS";
    stream.Open();
    stream.LoadFromFile(path);
    var content = stream.ReadText(-1);
    stream.Close();
    this.create_txt_viewing_file(view_file_path, content);

    return content
  },

  // txtの閲覧用ファイル作成
  create_txt_viewing_file: function(view_file_path, content)
  {
    var word = new ActiveXObject("Word.Application");
    var doc = word.Documents.Add();

    word.Selection.Text = content;
    word.Selection.Font.Size = 16;
    doc.SaveAs(view_file_path, 17);
    doc.Close(0);
    word.Quit();
  },

  // wordファイルの内容取得
  get_word_content: function(path, view_file_path)
  {
    var result = "";
    var word = WScript.CreateObject("Word.Application");
    var doc = word.Documents.Open(path);

    for (var i = 1; i <= doc.Paragraphs.Count; i++) {
      result += doc.Paragraphs(i).Range.Text;
    }

    doc.SaveAs(view_file_path, 17);
    doc.Close();
    word.Quit();

    return result;
  },

  //---------------------------------
  // ディレクトリに関する配列の作成
  //---------------------------------
  make_dirs_array: function(manuals, fso, folder, root_path, parent_id, depth, id_obj)
  {
    var dirs = new Enumerator(folder.SubFolders);
    var dir_count = 0;

    for (dirs.moveFirst(); !dirs.atEnd(); dirs.moveNext()) {
      var item = dirs.item();
      var dir_name =  item.Name;
      var path = item.Path;
      var dir_obj = this.get_obj(id_obj, parent_id, dir_count, dir_name, depth, this.get_relative_path(root_path, path), "dir");

      manuals.push(dir_obj);
      dir_count++;
      this.create_structure_array(manuals, path, fso, root_path, id_obj["id"], depth, id_obj);
    }
  },

  //==============================================
  // Finder側で読み込むjsファイルの作成
  //==============================================
  create_manuals_file: function(obj, dist, fso)
  {
    var json = JSON.stringify(obj);
    var content = "window.Manuals = " + json;
    var stream = new ActiveXObject("ADODB.Stream");
    var path = dist + "\\manuals.js";

    if(fso.FolderExists(dist) == false) {
      fso.CreateFolder(dist);
    }

    stream.Type = 2;
    stream.charset = "utf-8";
    stream.Open();
    stream.WriteText(content, 1);
    stream.SaveToFile(path, 2);
    stream.Close();
  }
}

ManualReloader.execute();
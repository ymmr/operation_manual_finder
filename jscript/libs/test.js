//===============================================================================================================================
// @outline テキストの内容を整形(ソート、空行等削除)する
// @language JScript
// @author P-774LSI
// @since 2014/03/01
//
// バイナリ読み込みコード以下のサイトのコードを流用しています
// ADODB.Streamによるファイルのバイナリアクセス（調査編）
// http://www.isla-plata.org/wiki/pukiwiki.php?ADODB.Stream%A4%CB%A4%E8%A4%EB%A5%D5%A5%A1%A5%A4%A5%EB%A4%CE%A5%D0%A5%A4%A5%CA%A5%EA%A5%A2%A5%AF%A5%BB%A5%B9%A1%CA%C4%B4%BA%BA%CA%D4%A1%CB
//===============================================================================================================================

//============================================================================================================================================================//
// Configuration                                                                                                                                              //
//============================================================================================================================================================//
// 文字エンコード処理
var init_read_charset;  // value = 読み込み時の文字コード。デフォルト(未定義状態)の場合、自動判別(精度難有り)
var init_write_charset;  /* value = 書き込み時の文字コード。デフォルト(未定義状態)の場合、BOM付きUTF-16(FE FF)、BOM付きUTF-16(FF FE)、
                       * BOM付きUTF-8のテキストはその文字コードで書き込み、それ以外の文字コードはShift-JISで書き込む。
                       * 使用可能な文字コード参考 http://msdn.microsoft.com/ja-jp/library/system.text.encodinginfo.name%28v=vs.110%29.aspx
                       */

// 行処理
var bln_remove_empty_line = false;  // true = 空行を取り除く
var bln_remove_line_with_only_space = false;  // true = スペースのみの行を取り除く
var bln_remove_line_with_only_em_space = false;  // true = 全角スペースのみの行を取り除く
var bln_remove_last_line_feed = true;  // true = 書き込み時、最終行の改行を取り除く

// ソート処理
var sort_type = 3;  /* value = ソートの種類, 0 = ソートを行わない, 1 = 組み込みソート(辞書正順), 2 = 組み込みソート(辞書逆順),
　　　　　　　　　　　      *  3 = 自然順ソート(正順), 4 = 自然順ソート(逆順)
                    */
var bln_lowercase_first = true;  // true = 比較した文字列が同じ場合、小文字の文字列を先に並べる

// 後処理
var bln_launch_notepad = true;  // true = ソート後にテキストをメモ帳で開く
var bln_processed_message = true  // true = 処理後にWScript.Echoで処理内容を表示

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//ここから下は変更不要 / no need to edit anything below here
var AD_TYPE_BINARY = 1;  // StreamTypeEnum / http://msdn.microsoft.com/ja-jp/library/cc389884.aspx
var AD_TYPE_TEXT = 2;
var AD_WRITE_CHAR = 0;  // StreamReadEnum / http://msdn.microsoft.com/ja-jp/library/cc389881.aspx
var AD_READ_ALL = -1;
var AD_WRITE_LINE = 1;  // StreamWriteEnum / http://msdn.microsoft.com/ja-jp/library/cc389886.aspx
var AD_CRLF = -1;  //  LineSeparatorsEnum /  http://msdn.microsoft.com/ja-jp/library/cc389826.aspx
var AD_SAVE_CREATE_OVER_WRITE = 2;  // SaveOptionsEnum / http://msdn.microsoft.com/ja-jp/library/cc389870.aspx

var stream = new ActiveXObject("ADODB.Stream");
var args = WScript.Arguments;
var read_charset;
var write_charset;

var text_path;
var arr = new Array();
var num_arr = new Array();
var str_arr = new Array();
var read_text;
var c;
var binary_data;
var delete_count;
var count;
var msg;
var delete_count_msg;
var count_msg;
var charset_msg;

var REPLSRC = new Array(
    0x20ac, 0x81,   0x201a, 0x192,
  0x201e, 0x2026, 0x2020, 0x2021,
 0x2c6,  0x2030, 0x160,  0x2039,
 0x152,  0x8d,   0x17d,  0x8f,
   0x90,   0x2018, 0x2019, 0x201c,
 0x201d, 0x2022, 0x2013, 0x2014,
 0x2dc,  0x2122, 0x161,  0x203a,
 0x153,  0x9d,   0x17e,  0x178
);

var REPLDST = new Array(
 0x80, 0x81, 0x82, 0x83, 0x84, 0x85, 0x86, 0x87,
 0x88, 0x89, 0x8a, 0x8b, 0x8c, 0x8d, 0x8e, 0x8f,
 0x90, 0x91, 0x92, 0x93, 0x94, 0x95, 0x96, 0x97,
 0x98, 0x99, 0x9a, 0x9b, 0x9c, 0x9d, 0x9e, 0x9f
);

//---------------------------------------------------------------------------------------------------------------------------------------------





//============================================================================================================================================================//
// Functions                                                                                                                                                  //
//============================================================================================================================================================//
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function checkBom(binary) {
 var r;

  if (binary.substring(0, 2) == "ef" && binary.substring(3, 5) == "bb" && binary.substring(6, 8) == "bf") {
       r = "utf-8"  // Unicode(UTF-8)
  } else if (binary.substring(0, 2) == "ff" && binary.substring(3, 5) == "fe") {
      r = "unicodeFFFE"  // Unicode(Littel-Endian)
    } else if (binary.substring(0, 2) == "fe" && binary.substring(3, 5) == "ff") {
      r = "unicodeFEFF"  // Unicode(Big-Endian)
   }

   return r;
}


// lowercase first argument
function compareLowercaseStrings (a, b) {
    var a_str = a.toString().toLowerCase();
    var b_str = b.toString().toLowerCase();

    if (a_str > b_str) {
        return 1;
    } else if (a_str < b_str) {
        return - 1;
    } else {
        if (a > b) {
            return - 1;
        } else if (a < b) {
            return 1;
        } else {
            return 0;
        }
    }
}


// Natural sort order
function natsort(array, lowercase_first) {  // second argument must be a boolean
    var compareSeparateStr = function (a, b) {
        if (a == b) { return 0; }

        var a_arr, b_arr, a_str, b_str, max_cnt, cnt, re;
        var bln_compare = true;

        re = /(\d+)|(\D+)/g;

        if  (a === "") {
            a_arr = [""];
       } else {
            a_arr = String(a).match(re);
        }

       if  (b === "") {
            b_arr = [""];
       } else {
            b_arr = String(b).match(re);
        }

        if (a_arr.length > b_arr.length) {
            max_cnt = b_arr.length;
        } else {
            max_cnt = a_arr.length;
        }


        for (var i = 0 ;  bln_compare && i <  max_cnt ; i ++) {
            if (a_arr[i] - b_arr[i]) {
                a_arr[i] -= 0;
                b_arr[i] -= 0;

                if (a_arr[i] > b_arr[i]) {    // numeric characters
                    bln_compare = false;
                    return 1;
                } else if (a_arr[i] < b_arr[i]) {
                    bln_compare = false;
                    return - 1;
                } else {
                    if (i == max_cnt - 1) {
                        if (a_arr.length > b_arr.length) {
                            return 1;
                        } else if (a_arr.length < b_arr.length) {
                            return - 1;
                        } else {
                            return 0;
                        }
                    }
                }
            } else {  // non numeric characters
                a_str = a_arr[i].toString().toLowerCase();
                b_str = b_arr[i].toString().toLowerCase();

                if (a_str > b_str) {
                    bln_compare = false;
                    return 1;
                } else if (a_str < b_str) {
                    bln_compare = false;
                    return - 1;
                } else {
                    if (i  == max_cnt - 1) {
                        if (a_arr.length > b_arr.length) {
                            return 1;
                        } else if (a_arr.length < b_arr.length) {
                            return - 1;
                        } else {  // The a_arr[i] and b_arr[i] elements have the same character string
                            if (lowercase_first) {
                                if (a_arr[i] > b_arr[i]) {
                                    return - 1;
                                } else if (a_arr[i] < b_arr[i]) {
                                    return 1;
                                } else {
                                    if (a > b) {
                                        return - 1;
                                    } else if (a < b) {
                                        return 1;
                                    } else {
                                        return 0;
                                    }
                                }
                            } else {
                                if (a_arr[i] > b_arr[i]) {
                                    return  1;
                                } else if (a_arr[i] < b_arr[i]) {
                                    return - 1;
                                } else {
                                    if (a > b) {
                                        return 1;
                                    } else if (a < b) {
                                        return - 1;
                                    } else {
                                        return 0;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    return array.sort(compareSeparateStr);
}





//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------





//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//============================================================================================================================================================//
// Process                                                                                                                                                                                                                                                                                                                                                  //
//============================================================================================================================================================//
for (var i = 0 ;  i < args.length ; i ++) {
  text_path = args(i);
 arr.length = 0;
 num_arr.length = 0;
 str_arr.length = 0;
 c = 0;
  binary_data = "";
   delete_count = 0;
   count = 0;
  msg = "";
   delete_count_msg = "";
  count_msg = "";
 charset_msg = "";

    stream.Type = AD_TYPE_TEXT;
 stream.charset = "iso-8859-1";
  stream.Open();
  stream.LoadFromFile(text_path)
  read_text = stream.readText(AD_READ_ALL);
   stream.close();


    for(var j = 0; j < read_text.length; j ++){
      c = read_text.charCodeAt(j);
        if(c > 0x100){   //変換がおかしいところは～0xffの範囲外
          for(var k = 0; k < REPLSRC.length; k ++){
                if (c == REPLSRC[k]){
                   c = REPLDST[k];
                 break;
              }
           }
       }
       binary_data += ((c < 0x10) ? "0" + c.toString(16) : c.toString(16));
     binary_data += " ";
     if (j % 16 == 15 && j >= 0xf) binary_data += "\n";
   }

   //WScript.Echo(binary_data)

 switch (true) {
     case (typeof checkBom(binary_data) === "undefined"):
            if (typeof init_read_charset === "undefined") { read_charset = "_autodetect_all"; }
         if (typeof init_write_charset === "undefined") { write_charset = "shift-jis"; }
         break;
      case (checkBom(binary_data) == "utf-8" ):
           if (typeof init_read_charset === "undefined") { read_charset = "utf-8"; }
           if (typeof init_write_charset === "undefined") { write_charset = "utf-8"; }
         break;
      case (checkBom(binary_data) == "unicodeFFFE" ):
         if (typeof init_read_charset === "undefined") { read_charset = "unicodeFFFE"; }
         if (typeof init_write_charset === "undefined") { write_charset = "unicodeFFFE"; }
           break;
      case (checkBom(binary_data) == "unicodeFEFF" ):
         if (typeof init_read_charset === "undefined") { read_charset = "unicodeFEFF"; }
         if (typeof init_write_charset === "undefined") { write_charset = "unicodeFEFF"; }
           break;
      default :
           if (typeof init_read_charset === "undefined") { read_charset = "_autodetect_all"; }
         if (typeof init_write_charset === "undefined") { write_charset = "shift-jis"; }
 }


   stream.Type = AD_TYPE_TEXT;
 stream.Charset = read_charset;
  stream.Open();
  stream.LoadFromFile(text_path)
  read_text = stream.readText(AD_READ_ALL);
   read_text = read_text.replace(/\r\n/g, "\n");
    arr = read_text.split("\n");



//// Processing of the line
 if (bln_remove_empty_line) {
        for (var l = 0 ;  l < arr.length ; l ++) {
           if (arr[l] === "") {
                arr.splice(l, 1);
               delete_count ++;
            }
       }
   }

   if (bln_remove_line_with_only_space) {
      for (var m = 0 ;  m < arr.length ; m ++) {
           if (arr[m].match(/^( )+$/)) {
               arr.splice(m, 1);
               delete_count ++;
            }
       }
   }

   if (bln_remove_line_with_only_em_space) {
       for (var n = 0 ;  n < arr.length ; n ++) {
           if (arr[n].match(/^(　)+$/)) {
               arr.splice(n, 1);
               delete_count ++;
            }
       }
   }



//// Processing of the sort
 switch (sort_type) {
        case 0:
         break;
      case 1:
         bln_lowercase_first ? arr.sort(compareLowercaseStrings) : arr.sort();
           break;
      case 2:
         bln_lowercase_first ? arr.sort(compareLowercaseStrings).reverse() : arr.sort().reverse();
           break;
      case 3:
         natsort(arr, bln_lowercase_first);
          break;
      case 4:
         natsort(arr, bln_lowercase_first).reverse();
            break;
      default :
   }


    stream.Close();

    stream.Charset = write_charset;
 stream.Open();

    //To move to the head of stream position for override.
  stream.Position = 0;  // Position property / http://msdn.microsoft.com/ja-jp/library/cc426742.aspx
  stream.SetEOS;  // SetEOS method / http://msdn.microsoft.com/ja-jp/library/jj250063.aspx


    for (var p = 0 ;  p < arr.length ; p ++) {
       if (bln_remove_last_line_feed && (p == arr.length - 1)) {
           stream.writeText(arr[p], AD_WRITE_CHAR);
            count ++;
       } else {
            stream.writeText(arr[p], AD_WRITE_LINE);
            count ++;
       }

    }

    stream.SaveToFile(text_path, AD_SAVE_CREATE_OVER_WRITE);
    stream.close();


 if (bln_launch_notepad) {
        var ws = new ActiveXObject("WScript.Shell");
        ws.Run("%windir%\\notepad.exe " + "\"" + args(i) + "\"");
       ws = null;
  }

    if (bln_processed_message) {
        charset_msg = "charset: " + read_charset + " -> " + write_charset + " \n";
       (delete_count == 1) ? delete_count_msg = "1 item deleted \n" : (delete_count >= 2) ? delete_count_msg = delete_count + " items deleted \n" : delete_count_msg = "";
      (count >= 2) ? count_msg = count + " items sorted \n" : count_msg = "";
      msg += charset_msg + count_msg + delete_count_msg + "(" + text_path + ") \n";
      WScript.Echo("complete sort \n" + msg);
    }

}
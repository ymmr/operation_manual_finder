function dump(data, nest)
{
  if((typeof(data) == "string") || (typeof(data) == "number")) {
    return data;
  }

  nest = nest ? nest : 0;
  var indent = "";
  var result = "";

  for (var i = 0; i < nest; i++) {
    indent = indent + '  ';
  }

  for (var i in data) {
    if (typeof(data[i]) == 'object') {
      result = result + indent + i.toString() + ": \n" + dump(data[i], nest + 1);
    } else {
      result = result + indent + i.toString() + ": " + data[i].toString() + "\n";
    }
  }

  return result;
}
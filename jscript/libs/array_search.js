function array_search(array, val)
{
  var result = -1;

  for(var i = 0; i < array.length; i++) {
    if(array[i] === val.toLowerCase()) {
      result = i;
      break;
    }
  };

  return result;
}
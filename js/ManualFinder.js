$(function()
{
  "use strict";
  //================================
  //
  // 総合
  //
  //================================
  class ManualFinder {
    constructor(manuals)
    {
      this.$page = $("#ManualFinder");

      this.init(manuals);
    }

    //============================
    // 初期化
    //============================
    init(manuals)
    {
      if(manuals) {
        this.setup_html(manuals);
        this.setup_events(manuals);
        this.setup_renders(manuals);
      } else {
        alert("マニュアルが見つかりませんでした。\nManualReloader.wsfを実行後リロードしてください。");
      }
    }

    setup_html(manuals)
    {
      const html_creator = new HtmlCreator(manuals);

      $(".mn-Selector", this.$page).html(html_creator.selector_html);
      $(".mn-Breadcrumb", this.$page).html(html_creator.breadcrumb_html);
      $(".mn-WordSearch", this.$page).html(html_creator.word_search_html);
    }

    setup_events(manuals)
    {
      this.on_change_window();
      this.on_click_item(manuals);
      this.on_click_prev_items(manuals);
      this.on_hover_active_dir();
      this.on_hover_prev_items();
      this.on_hover_item(manuals);
      this.on_keydown(manuals);
      this.on_focus_word_search_box();
      this.on_change_word_search_box();
      this.on_click_word_search_icon(manuals);
      this.on_click_word_search_close_button();
    }

    on_change_window()
    {
      const callbacks = [
        this.change_search_result_size,
      ].map((callback) => {
        return callback.bind(this);
      });

      new WindowEventHandler(callbacks);
    }

    setup_renders(manuals)
    {
      const root_id = this.get_root_id(manuals);

      this.reflesh_hisory(root_id, manuals);
    }

    //============================
    // 値の取得
    //============================
    get_root_id(manuals)
    {
      return manuals[0].parent_id;
    }

    get_words_array(val)
    {
      return val.match(/\S+/g).filter((elm, i, self) => {
        return self.indexOf(elm) === i;
      });
    }

    get_file_path(path)
    {
      const root_path = location.pathname.match(/(.*)\/|\\$/g)[0];

      if(root_path.match(/\/$/)) {
        path = path.replace(/\\/g, "/");
      }

      return root_path + path;
    }

    //============================
    // イベント
    //============================
    //---------------------------------
    // キー押下時
    //---------------------------------
    on_keydown(manuals)
    {
      $(document).on("keydown", (evt) => {
        switch(evt.keyCode) {
          case 13:
            this.action_enter_key(manuals);
            break;
          case 27:
            this.action_esc_key(manuals);
            break;
          default:
        }
      });
    }

    // エンターキー
    action_enter_key(manuals)
    {
      const is_active = $(".mn-WordSearch_BoxIcon", this.$page).hasClass("active");
      const $box = $(".mn-WordSearch_Box", this.$page);
      const val = $box.val();

      if(is_active && $box[0] == document.activeElement) {
        this.change_word_search_result(val, manuals);
      }
    }

    // escキー
    action_esc_key(manuals)
    {
      const is_show_text_file = $(".mn-Display", this.$page).hasClass("show");
      const is_show_search_result = $(".mn-WordSearch", this.$page).hasClass("show-result");

      if(is_show_text_file) {
        this.hide_display();
      } else if(is_show_search_result) {
        this.hide_word_search_result();
      } else {
        this.rewind_history(manuals);
      }
    }

    //---------------------------------
    // 検索アイコンアイコンフォーカス時
    //---------------------------------
    on_focus_word_search_box()
    {
      $(document).on("focus", ".mn-WordSearch_Box", (evt) => {
        $(evt.currentTarget).select();
      });
    }

    //---------------------------------
    // 検索アイコン入力時
    //---------------------------------
    on_change_word_search_box()
    {
      $(document).on("keyup blur", ".mn-WordSearch_Box", (evt) => {
        const $box = $(evt.currentTarget);
        const $icon = $box.siblings(".mn-WordSearch_BoxIcon");
        const val = $box.val();
        const class_name = "active";

        if(val.match(/\S+/g)) {
          $icon.addClass(class_name);
        } else {
          $icon.removeClass(class_name);
        }
      });
    }

    //---------------------------------
    // 検索アイコンクリック時
    //---------------------------------
    on_click_word_search_icon(manuals)
    {
      $(document).on("click", ".mn-WordSearch_BoxIcon.active", (evt) => {
        const $box = $(evt.currentTarget).siblings(".mn-WordSearch_Box");
        const val = $box.val();

        this.change_word_search_result(val, manuals);
      });
    }

    //---------------------------------
    // 前の階層をクリック時
    //---------------------------------
    on_click_prev_items(manuals)
    {
      $(document).on("click", ".mn-Selector_Layer.prev .mn-Selector_Items.show", () => {
        this.rewind_history(manuals);
      });
    }

    //---------------------------------------
    // 選択中の階層の各アイテムをクリック時
    //---------------------------------------
    on_click_item(manuals)
    {
      $(document).on("click", ".mn-Trigger", (evt) => {
        evt.preventDefault();

        const id = $(evt.currentTarget).data("id");
        const selected_item = manuals.filter((item) => {
          return item.id == id;
        })[0];

        if(selected_item) {
          this.check_clicked_item_type(selected_item, manuals);
        } else {
          this.reflesh_hisory(id, manuals);
        }

      });
    }

    // アイテムの種類の仕分け
    check_clicked_item_type(selected_item, manuals)
    {
      switch(selected_item.type) {
        case "dir":
          this.reflesh_hisory(selected_item.id, manuals);
          this.hide_word_search_result();
          break;
        case "file":
          this.divide_file_reading(selected_item, manuals);
          break;
        default:
          console.error("Unknown Item Type");
      }
    }

    // ファイルの閲覧方法の分岐
    divide_file_reading(selected_item, manuals)
    {
      if(selected_item.attributes.is_supported) {
        window.open(selected_item.attributes.view_file_path);
      }
    }

    //----------------------------------------
    // 選択中の階層のフォルダitemのホバー時
    //----------------------------------------
    on_hover_active_dir()
    {
      $(document).on("mouseenter mouseleave", ".mn-Selector_Layer.active .mn-Selector_Link",(evt) => {
        const evt_type = evt.type;
        const id =  $(evt.currentTarget).data("id");

        this.change_next_items(id, evt_type);
      });
    }

    //--------------------------------------
    // 前の階層のホバー時
    //--------------------------------------
    on_hover_prev_items()
    {
      $(document).on("mouseenter mouseleave", ".mn-Selector_Layer.prev .mn-Selector_Items.show",(evt) => {
        const evt_type = evt.type;
        const $self = $(evt.currentTarget);
        const $link = $(".mn-Selector_Link", $self);
        const class_name = "hover";
        const margin_left = parseInt($self.closest(".mn-Selector_Layer").css("margin-left"));

        if((evt_type == "mouseenter") && (margin_left == 0)) {
          $link.addClass(class_name);
        } else {
          $link.removeClass(class_name);
        }
      });
    }

    //--------------------------------------
    // アイテムホバー時
    //--------------------------------------
    on_hover_item(manuals)
    {
      $(document).on("mouseenter mouseleave", ".mn-Trigger",(evt) => {
        const evt_type = evt.type;
        const $target_item = $(evt.currentTarget);
        const id = $(evt.currentTarget).data("id");
        const selected_item = manuals.filter((item) => {
          return item.id == id;
        })[0];

        if(selected_item) {
          switch(selected_item.type) {
            case "file":
            this.change_incompatible_message(evt_type, selected_item);
            return;
            default:
          }
        }
      });
    }

    //--------------------------------------
    // 検索閉じるクリック時
    //--------------------------------------
    on_click_word_search_close_button()
    {
      $(document).on("click", ".mn-WordSearch_CloseButton", (evt) => {
        this.hide_word_search_result()
      });
    }

    //============================
    // 描画に関する関数
    //============================
    // 前の階層に戻る
    rewind_history(manuals)
    {
      const $active_layer = $(".mn-Selector_Layer.active", this.$page);
      const $active_items = $(".mn-Selector_Items.show", $active_layer);
      const selected_id = $active_items.data("parent-id");
      const item = manuals.filter((elm) => {
        return elm.id == selected_id;
      })[0];

      if(item) {
        this.reflesh_hisory(item.parent_id ,manuals);
      }
    }

    // 履歴の更新
    reflesh_hisory(id, manuals)
    {
      const history = [];

      this.create_history(id, manuals, history);
      this.change_layer(history);
      this.change_selected_item(history);
      this.change_breadcrumb(manuals, history);
      this.change_reset_button(history);
      this.change_layers_height();
    }

    // 履歴の作成
    create_history(id, manuals, history) {
      history.unshift(id);

      const parents = manuals.filter((elm) => {
        return elm.id == id
      });

      if(parents.length !== 0) {
        const parent_id = parents[0].parent_id;
        this.create_history(parent_id, manuals, history);
      }
    }

    // layerの描画変更
    change_layer(history)
    {
      const $active_items = $(".mn-Selector_Items[data-parent-id='" + history[history.length - 1] + "']", this.$page);
      const $active = $active_items.closest(".mn-Selector_Layer");
      const $next = $active.next();
      const $prev = $active.prev();
      const $past = $active.prevAll().not($prev);

      $(".mn-Selector_Layer", this.$page).removeClass("prev next active open past");

      $past.addClass("past");
      $active.addClass("active open");
      $next.addClass("next open");
      $prev.addClass("prev open");
    }

    // 選択されたitemsの描画変更
    change_selected_item(history)
    {
      $(".mn-Selector_Item", this.$page).removeClass("selected");
      $(".mn-Selector_Items", this.$page).removeClass("show");

      for(let key in history) {
        const id = history[key];
        $(".mn-Selector_Items[data-parent-id='" + id +"']", this.$page).addClass("show");
        $(".mn-Selector_Link[data-id='" + id +"']", this.$page).closest(".mn-Selector_Item").addClass("selected");
      }
    }

    //パンくずの描画変更
    change_breadcrumb(manuals, history)
    {
      $(".mn-Breadcrumb_Layer", this.$page).removeClass("active");
      $(".mn-Breadcrumb_Item", this.$page).removeClass("selected current");

      for(let key in history) {
        const id = history[key];
        const $item = $(".mn-Breadcrumb_Link[data-id='" + id + "']", this.$page).closest(".mn-Breadcrumb_Item");
        const $layer = $item.closest(".mn-Breadcrumb_Layer");

        $layer.addClass("active");
        $item.addClass("selected");

        if(id == history[history.length - 1]) {
          $item.addClass("current");
        }
      }
    }

    // リセットボタンの描画変更
    change_reset_button(history)
    {
      const $reset_button = $(".mn-Breadcrumb_ResetButton", this.$page);
      const class_name = "active";

      if(history.length > 1) {
        $reset_button.addClass(class_name);
      } else {
        $reset_button.removeClass(class_name);
      }
    }

    // 次の階層のitemsの描画
    change_next_items(parent_id, evt_type) {
      const $target = $(".mn-Selector_Items[data-parent-id='" + parent_id + "']", this.$page);
      const $siblings = $target.parent().children().not($target);
      const class_name = "show";

      $siblings.removeClass(class_name);

      if(evt_type == "mouseenter") {
        $target.addClass(class_name);
      } else {
        $target.removeClass(class_name);
      }
    }

    // はみ出しでoverflow変更
    change_wrapper_overflow($wrapper, $content)
    {
      if($wrapper.offsetHeight < $content.offsetHeight) {
        $wrapper.css("overflow", "scroll");
      } else {
        $wrapper.css("overflow", "auto");
      }
    }

    // 検索結果の変更
    change_word_search_result(val, manuals)
    {
      const words = this.get_words_array(val)
      const matches = new WordSearcher(words, manuals).matches;
      const html = new HtmlCreator(matches).word_search_result_html;
      const $result = $(".mn-WordSearch_Result", this.$page);

      $result.html(html);
      $(".mn-WordSearch_Val", this.$page).text("\"" + words.join(" ") + "\"");

      this.change_wrapper_overflow($result, $result.children());
      this.show_word_search_result();
    }

    show_word_search_result()
    {
      const $word_search = $(".mn-WordSearch", this.$page);
      const class_name = "show-result";
      const scroll_controler = new ScrollControler;

      if(!$word_search.hasClass(class_name)) {
        $word_search.addClass(class_name);
        this.change_search_result_size();
      }
    }

    hide_word_search_result()
    {
      const $word_search = $(".mn-WordSearch", this.$page);
      const class_name = "show-result";
      const scroll_controler = new ScrollControler;

      if($word_search.hasClass(class_name)) {
        $word_search.removeClass(class_name);
      }
    }

    // 非対応メッセージの表示変更
    change_incompatible_message(evt_type, selected_item)
    {
      const path = this.get_file_path(selected_item.path);
      const $word_search = $(".mn-WordSearch", this.$page);
      const is_show_word_search = $word_search.hasClass("show-result");
      const $wrapper = is_show_word_search ? $word_search : $(".mn-Breadcrumb", this.$page);
      const class_name = "show-message";

      if(evt_type == "mouseenter" && !selected_item.attributes.is_supported) {
        $(".mn-Message_Path", $wrapper).html(path);
        $wrapper.addClass(class_name);
      } else {
        $("." + class_name, this.$page).removeClass(class_name);
      }
    }

    //ページの高さ調節
    change_layers_height()
    {
      const $layers = $(".mn-Selector_Layers", this.$page);
      const $active_layer = $(".mn-Selector_Layer.active", $layers);
      const active_layer_h = $active_layer.height();

      if(active_layer_h >= $layers.height()) {
        $layers.css("min-height", active_layer_h + "px");
      }
    }

    //検索結果の高さ調整
    change_search_result_size(evt)
    {
      const is_result_show = $(".mn-WordSearch", this.$page).hasClass("show-result");
      if((!evt || (evt.type == "resize")) && is_result_show) {
        const $body = $(".mn-WordSearch_Body", this.$page);
        const $result = $(".mn-WordSearch_Result", $body);
        const $button = $(".mn-WordSearch_CloseButton", $body);
        const offset = $body[0].offsetTop;
        const button_h = $button[0].offsetHeight + parseInt($button.css("margin-top")) * 2;
        const adjust = offset + button_h;
        const scroll_bottom = window.scrollY + window.innerHeight;
        const val = window.innerHeight - adjust;

        $result.css("height", val + "px");
      }
    }
  }

  new ManualFinder(window.Manuals);
});
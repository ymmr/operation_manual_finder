class HtmlCreator {
  constructor(manuals)
  {
    this.manuals = manuals;
    this.data_refiner = new DataRefiner(manuals);
  }

  //=============================
  // PUBLIC
  //=============================
  // ワード検索
  get word_search_html()
  {
    const name_space = "mn-WordSearch";

    return this.get_word_search_html(name_space);
  }

  // ワード検索結果
  get word_search_result_html()
  {
    const name_space = "mn-WordSearch";

    return this.get_word_search_result_html(name_space);
  }

  // セレクタ
  get selector_html()
  {
    const name_space = "mn-Selector";

    return this.get_selector_html(name_space);
  }

  // パンくず
  get breadcrumb_html()
  {
    const name_space = "mn-Breadcrumb";
    const $reset_button = $("<button>")
          .addClass(name_space + "_ResetButton mn-Trigger")
          .attr("data-id", 0);

    return [this.get_incompatible_massage_html(name_space), $reset_button, this.get_selector_html(name_space)];
  }

  // テキストディスプレイ
  get display_html()
  {
    const name_space = "mn-Display";
    const $close_button = $("<button>").addClass(name_space + "_CloseButton");

    return [$close_button, this.get_display_html(name_space)];
  }

  // 検索結果
  get word_search_result_html()
  {
    const name_space = "mn-WordSearch";

    return this.get_word_search_result_html(name_space);
  }

  //-----------------------------
  // メッセージhtml
  //-----------------------------
  get_incompatible_massage_html(name_space)
  {
    const $path = $("<span>").addClass("mn-Message_Path");
    const $caution = $("<span>")
          .addClass(name_space + "_IncompatibleMessage mn-Massage")
          .html(["ビュー非対応のファイルです。直接ファイルを開いてください。<br/>", $path]);

    return $caution;
  }

  //-----------------------------
  // セレクタ・パンくず共用html
  //-----------------------------
  get_selector_html(name_space)
  {
    const layers_html = this.get_layers_html(name_space);
    const $layers = $("<ol>")
          .addClass(name_space + "_Layers")
          .html(layers_html);

    return $layers;
  }

  get_layers_html(name_space)
  {
    const selector_data = this.data_refiner.selector_data;
    let layers_html = [];

    for(let depth in selector_data) {
      const layer = selector_data[depth];
      const layer_html = this.get_layer_html(layer, name_space);
      const $layer = $("<li>")
            .addClass(name_space + "_Layer")
            .html(layer_html);

      layers_html.push($layer);
    }

    return layers_html;
  }

  get_layer_html(layer, name_space)
  {
    let layer_html = [];

    for(let parent_id in layer) {
      const items = layer[parent_id];
      const items_html = this.get_items_html(items, name_space);
      const $items = $("<ol>")
            .addClass(name_space + "_Items")
            .attr("data-parent-id", parent_id)
            .html(items_html);

      layer_html.push($items);
    }

    return layer_html;
  }

  get_items_html(items, name_space)
  {
    let items_html = [];

    items.map((item) => {
      const item_html = this.get_item_html(item, name_space);
      const $item = $("<li>")
            .addClass(name_space + "_Item mn-TriggerItem")
            .attr("data-type", item.type)
            .html(item_html);

      items_html.push($item);
    })

    return items_html;
  }

  get_item_html(item, name_space)
  {
    const $title = $("<h3>")
          .addClass(name_space + "_Title mn-ItemTitle")
          .html(item.title);
    const $link = $("<a></a>")
          .addClass(name_space + "_Link mn-Trigger")
          .attr({
            "data-id": item.id
          })
          .html($title);

    if(item.type == "file") {
      this.create_file_link($link, item, name_space);
    }

    return $link;
  }

  create_file_link($link, item, name_space)
  {
    $link.attr({
      "href": item.attributes.view_file_path,
      "data-is_supported": item.attributes.is_supported
    });

    return $link
  }

  //----------------------------------------
  // ワード検索のhtml取得
  //----------------------------------------
  get_word_search_html(name_space)
  {
    const $header = $("<header>")
          .addClass(name_space + "_Header")
          .html(this.get_word_search_header_html(name_space));
    const $body = $("<div>")
          .addClass(name_space + "_Body")
          .html(this.get_word_search_body_html(name_space));

    return [$header, $body];
  }

  get_word_search_header_html(name_space)
  {
    const $text_box = $("<input>")
          .addClass(name_space + "_Box")
          .attr("type", "text");

    const $icon = $("<button>").addClass(name_space + "_BoxIcon");

    return [$text_box, $icon];
  }

  get_word_search_body_html(name_space)
  {
    const $val = $("<span>").addClass(name_space + "_Val");
    const $result = $("<div>").addClass(name_space + "_Result");
    const $close_button = $("<button>").addClass(name_space + "_CloseButton");

    return [this.get_incompatible_massage_html(name_space), $val, $result, $close_button];
  }

  //----------------------------------------
  // ワード検索結果のhtml取得
  //----------------------------------------
  get_word_search_result_html(name_space)
  {
    if(this.manuals.length !== 0) {
      const item_html = this.get_items_html(this.manuals, name_space);
      const $items = $("<ol>")
            .addClass(name_space + "_Items")
            .html(item_html);

      return $items;
    } else {
      return $("<span>").addClass(name_space + "_NotFound").text("Not Found.")
    }
  }
}
//================================
//
// データの加工
//
//================================
class DataRefiner {
  constructor(manuals)
  {
    this.manuals = manuals;
  }

  //================================
  // テキストデータ
  //================================
  get text_files_data()
  {
    return this.manuals.filter((item) => {
      return (item.type == "file") && (item.attributes.body !== "");
    });
  }

  //================================
  // セレクタデータ
  //================================
  get selector_data()
  {
    let selector_data = {};

    this.manuals.map((item) => {
      if(!selector_data[item.depth]) {
        selector_data[item.depth] = {};
      }

      if(item.type == "dir" && !selector_data[item.depth + 1]) {
        selector_data[item.depth + 1] = {};
      }

      if(item.type == "dir") {
        selector_data[item.depth + 1][item.id] = [];
      }

      if(!selector_data[item.depth][item.parent_id]) {
        selector_data[item.depth][item.parent_id] = [];
      }

      selector_data[item.depth][item.parent_id].push(item);
    });

    return selector_data;
  }
}

class WordSearcher {
  constructor(words, manuals)
  {
    this.manuals = manuals;
    this.words = words;
  }

  get matches()
  {
    return $.extend(true, [], this.manuals).filter((manual) => {
      let words_matches = [];
      for(let key in this.words) {
        const word = this.words[key].toLowerCase();
        const reg = new RegExp(word, "i");
        const title_match = manual.title.match(reg);
        const body_match = manual.attributes.body ? manual.attributes.body.match(reg) : null;

        words_matches.push(title_match || body_match);

        manual.title = title_match ? this.get_highlighted_char(manual.title, word) : manual.title;
        manual.attributes.body = body_match ? this.get_highlighted_char(manual.attributes.body, word) : manual.attributes.body;
      }

      return $.inArray(null, words_matches) == -1;
    });
  }

  get_highlighted_char(char, word)
  {
    const tag_name = "strong";
    const tag_start = "<" + tag_name + ">";
    const tag_end = "</" + tag_name + ">";
    const reg = new RegExp(tag_start + ".*?" + tag_end, "g");
    const matches = char.match(reg);
    const new_char = char.split(reg).map((elm, i) => {
      elm = elm.replace(new RegExp("(" + word + ")", "gmi"), tag_start + "$1" + tag_end);

      if(matches && matches[i]) {
        elm = matches[i] + elm;
      }

      return elm;
    });

    return new_char.join("");
  }
}
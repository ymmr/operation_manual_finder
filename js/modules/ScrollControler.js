class ScrollControler {
  constructor()
  {
  }

  stop()
  {
    $("body").css({
      "overflow": "hidden",
      "padding-right": this.get_bar_w() + "px"
    });
  }

  start()
  {
    $("body").removeAttr("style");
  }

  get_bar_w()
  {
    return window.innerWidth - document.body.clientWidth;
  }
}
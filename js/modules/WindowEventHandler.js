class WindowEventHandler {
  constructor(callbacks) {
    this.init(callbacks);
  }

  init(callbacks)
  {
    const fps = 60;
    let do_event = false;

    $(window).on("resize scroll load", (evt) => {

      if(do_event) {
        return;
      } else {
        for(let key in callbacks) {
          callbacks[key](evt);
        }

        do_event = true;

        setTimeout(() => {
          do_event = false;
        }, 1000/fps);
      }
    });
  }
}